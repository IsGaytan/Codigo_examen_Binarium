package numeros_faltante;

/**
 *
 * @author Is Gaytán
 */
import java.util.Arrays;
import javax.swing.*;
public class Numeros_faltante {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Pedir la cantidad de números a ingresar
        int cantidad= Integer.parseInt(JOptionPane.showInputDialog("Cuántos número desea ingresar?"));
        
        //Crear el array para ingresa los números
        int[] numeros = new int [cantidad];
        
        //Ingresar los números al array
        for(int i = 0; i<cantidad; i++)
        {
            int valor = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número en la posición " + (i + 1)));
            numeros [i] = valor;
        }
        
        //Ordenar el array de manera ascendente
        Arrays.sort(numeros);
        int primer_num = 0;
        int numero_com = 0;
        
        //Recorrer el array para buscar el número que falta
        for(int i = 0; i<cantidad; i++)
        {
            //Tomamos el primer valor del array (el valor más chico) si no es el más chico, toma el que sigue
            if(i == 0)
            {
                primer_num= numeros[i];
                JOptionPane.showMessageDialog(null, primer_num);
                continue;
            }
            else
            {
                numero_com = numeros[i];
                JOptionPane.showMessageDialog(null, numero_com);
                
                //Si el número actual NO es igual a la sumatoria que converite el número más chico en igual a este número
                //Muestra mensaje del número que falta y termina el programa.
                if(numero_com == (primer_num+1+(i-1)))
                {
                    //JOptionPane.showMessageDialog(null, "El " + numero_com + " es continuo a " + primer_num);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Falta el " + (primer_num+1+(i-1)));
                    break;
                }
            }
            
            
            
            
        }
    }
    
}
