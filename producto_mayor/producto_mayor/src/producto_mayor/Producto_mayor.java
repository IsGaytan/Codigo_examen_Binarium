/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package producto_mayor;

/**
 *
 * @author Is Gaytán
 */
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;
public class Producto_mayor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Solicitar la cantidad de números a evaluar
        int cantidad = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la canidad de números a evaluar: "));
        
        //Crear el array que guardará los números y el arraylist que guardará el resultado de los productos
        int[] numeros = new int [cantidad];
        ArrayList<Integer> productos = new ArrayList<Integer>();
        int valor = 0;
        
        //Ingresar los valores por el usuario a evaluar
        for(int i = 0; i<cantidad; i++)
        {
            int num = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el número en la posición " + (i + 1)));
            numeros [i] = num;
        }
        
        //Tomar esos valores para multiplicarlos entres sí y guardar sus productos
        for(int i = 0; i < cantidad; i++)
        {
            valor = numeros[i];
            
            for(int j = 0; j < cantidad; j++)
            {
                if(numeros[i] == numeros[j])
                {
                    //JOptionPane.showMessageDialog(null, "Son iguales los coeficientes.");
                }
                else
                {
                    productos.add(numeros[i]*numeros[j]);
                }
                
            }
            
        }
        
        //Ordenar el array list con los productos
        Collections.sort(productos);
        
        //Recorrer el arraylist de mayor a menor para ir evaluando el residuo entre 3 para que de cero
        for(int h = productos.size(); h>0; h--)
        {
            int dividendo = productos.get(h-1);
            int residuo = dividendo%3;
            if(residuo == 0)
            {
                JOptionPane.showMessageDialog(null, "El resultado: " + dividendo);
                break;
            }
            //JOptionPane.showMessageDialog(null, "EL DIVIDENDO: " + productos.get(h-1) + " El Residuo " + residuo);
        }
    }
    
}
